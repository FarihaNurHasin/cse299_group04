<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Illuminate\Support\Str;
class Classroom extends Model
{
    use HasFactory;
    protected $fillable = [
        'classroom',
        'section',
        'code',
        'description',
        
    ];
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    public function contents()
    {
        return $this->hasMany(Content::class);
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $random = Str::random(4);
            $model->code = IdGenerator::generate(['table'=>'classrooms', 'field'=>'code', 'length' => 10, 'prefix' =>'CLS-']);
            $model->code = $model->code."-".$random;
        });
    }
}
