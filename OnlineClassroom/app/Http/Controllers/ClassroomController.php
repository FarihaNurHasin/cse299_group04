<?php

namespace App\Http\Controllers;
use App\Models\Classroom;
use Illuminate\Http\Request;
use App\Models\Content;
class ClassroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classrooms = auth()->user()->classrooms()->get();
        return view('classroom.index')->with(['classrooms' => $classrooms]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classroom.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $request->validate([
            'classroom' => 'required|string|max:255',
            'section' => 'required|int|max:255',
            'description' => 'required|string|max:255',
           
        ]);

        auth()->user()->classrooms()->create($request->all());
        return redirect('classroom');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Classroom $classroom)
    {   
        $contents = $classroom->contents()->orderBy('created_at', 'desc')->get();
        return view('classroom.show', compact('classroom', 'contents')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Classroom $classroom)
    {
        return view('classroom.edit', compact('classroom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classroom $classroom)
    {
        $request->validate([
            'classroom' => 'required|string|max:255',
            'section' => 'required|int|max:255',
            'description' => 'required|string|max:255'
        ]);

        $classroom->update($request->all());

        return redirect()->route('classroom.index')
            ->with('success', 'Classroom updated successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classroom $classroom)
    {
        $classroom->delete();
        return redirect('classroom');
    }
    public function join()
    {
        return view('classroom.join');
    }

    public function add(Request $request)
    {
        $join = Classroom::where('code', $request->input('code'))->get();

        auth()->user()->classrooms()->attach($join->pluck('id'));
        return redirect('classroom');
    }
}
