<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route :: group(['middleware'=> 'auth'], function(){
    Route::get('/classroom/join', [ClassroomController::class, 'join'])->name('classroom.join');
    Route::post('/classroom/join', [ClassroomController::class, 'add'])->name('classroom.add');
    Route::resource('classroom', ClassroomController::class);
    Route::resource('content', ContentController::class);
    Route::group(['middleware' => 'role:student','prefix' => 'student', 'as' => 'student.'],function(){
        Route ::resource('lessons',\App\Http\Controllers\Students\LessonController::class);
        
    });
Route::group(['middleware' => 'role:teacher','prefix' => 'teacher', 'as' => 'teacher.'],function(){
    Route ::resource('courses',\App\Http\Controllers\Teachers\CourseController::class);
    
});


});

require __DIR__.'/auth.php';
